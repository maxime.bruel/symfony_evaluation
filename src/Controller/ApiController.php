<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use App\Entity\Pokemon;
use App\Entity\Type;
use App\Repository\PokemonRepository;
use App\Repository\TypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    #[Route('/api', name: 'app_api')]
    public function index(): Response
    {
        return $this->json([
            'message' => 'API documentation',
            'list of pokemons' => [
                '/api/pokemon',
                '/api/pokemon?type={label}',
                '/api/pokemon?limit=10',
                '/api/pokemon?offset=0',
            ],
            'single pokemon' => [
                '/api/pokemon/{uid}',
            ],
            'list of types' => [
                '/api/type',
            ],
            'single type' => [
                '/api/type/{uid}',
            ],
        ]);
    }

    #[Route('/api/pokemon', name: 'api_pokemon_get', methods: ["GET"])]
    public function indexPokemonGet(PokemonRepository $pokemonRepository, TypeRepository $typeRepository, Request $request): Response
    {
        $limit = $request->get('limit');
        if (empty($limit)) {
            $limit = 10;
        }
        $offset = $request->get('offset');
        if (empty($offset)) {
            $offset = 0;
        }
        $type = $request->get('type');
        if (!empty($type)) {
            return $this->json($typeRepository->findOneBy(['label' => $type])->getPokemon(), 200, [], ['groups' => 'pokemon:read']);
        }
        return $this->json($pokemonRepository->findBy([], [], $limit, $offset), 200, [], ['groups' => 'pokemon:read']);
    }

    #[Route('/api/pokemon/{uid}', name: 'api_pokemon_get_id', methods: ["GET"])]
    public function indexPokemonGetId(Pokemon $pokemon, PokemonRepository $pokemonRepository): Response
    {
        return $this->json($pokemonRepository->find($pokemon), 200, [], ['groups' => 'pokemon:read']);
    }

    #[Route('/api/type', name: 'api_type_get', methods: ["GET"])]
    public function indexTypeGet(TypeRepository $typeRepository): Response
    {
        return $this->json($typeRepository->findAll(), 200, [], ['groups' => 'type:read']);
    }

    #[Route('/api/type/{uid}', name: 'api_type_get_id', methods: ["GET"])]
    public function indexTypeGetId(Type $type, TypeRepository $typeRepository): Response
    {
        return $this->json($typeRepository->find($type), 200, [], ['groups' => 'type:read']);
    }
}
