<?php

namespace App\DataFixtures;

use App\Entity\Pokemon;
use App\Entity\Type;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Uid\Uuid;

class AppFixtures extends Fixture
{

    public function load(ObjectManager $manager): void
    {
        // Pokemon
        for ($i = 1; $i < 21; $i++) {
            $pokemon = new Pokemon();
            $pokemon->setName('pokemon-' . $i);
            $pokemon->setDescription('Lorem ipsum ' . $i);
            $pokemon->setUid(Uuid::v6()->toBase32());
            $manager->persist($pokemon);
        }

        // Type
        for ($i = 1; $i < 6; $i++) {
            $type = new Type();
            $type->setLabel('type-' . $i);
            $type->setUid(Uuid::v6()->toBase32());
            $manager->persist($type);
        }

        $manager->flush();
    }
}
